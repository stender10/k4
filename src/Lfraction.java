import java.util.*;

/** This class represents fractions of form n/d where n and d are long integer 
 * numbers. Basic operations and arithmetics for fractions are provided.
 */
public class Lfraction implements Comparable<Lfraction> {

   /** Main method. Different tests. */
   public static void main (String[] param) {
      Lfraction f1 = new Lfraction (2, 5);
      System.out.println(f1.greatestCommonDivisor(-2, 5));
      System.out.println(f1.greatestCommonDivisor(-4, 7));

      String abc = "1234";
      f1.equals(abc);

//      Lfraction f1 = new Lfraction (2, 5);
//      Lfraction f2 = new Lfraction (4, 7);
//      Lfraction f3 = new Lfraction(0, 0);
//
//      System.out.println(valueOf(f1.toString()));
//      System.out.println(valueOf("3 / 7"));
//      System.out.println(valueOf("3 * 7"));
   }

   private long numerator;
   private long denominator;

   /** Constructor.
    * @param a numerator
    * @param b denominator > 0
    */
   public Lfraction (long a, long b) {
      if (b == 0) throw new RuntimeException("\nDenominator != 0");

      long gcd = greatestCommonDivisor(a, b);
      if (b < 0) {
         numerator = -a / gcd;
         denominator = -b / gcd;
      } else {
         numerator = a / gcd;
         denominator = b / gcd;
      }
   }

   /** Public method to access the numerator field. 
    * @return numerator
    */
   public long getNumerator() {
      return numerator;
   }

   /** Public method to access the denominator field. 
    * @return denominator
    */
   public long getDenominator() { 
      return denominator;
   }

   /** Conversion to string.
    * @return string representation of the fraction
    */
   @Override
   public String toString() {
      return numerator + " / " + denominator;
   }

   /** Equality test.
    * @param m second fraction
    * @return true if fractions this and m are equal
    */
   @Override
   public boolean equals (Object m) {
      if (m == this) return true;
      if (!(m instanceof Lfraction)) return false;

      Lfraction c = (Lfraction) m;
      return compareTo(c) == 0;
   }

   /** Hashcode has to be equal for equal fractions.
    * @return hashcode
    */
   @Override
   public int hashCode() {
      return Objects.hash(numerator, denominator);
   }

   /** Sum of fractions.
    * @param m second addend
    * @return this+m
    */
   public Lfraction plus (Lfraction m) {
      long a = numerator * m.denominator + m.numerator * denominator;
      long b = denominator * m.denominator;
      return new Lfraction(a, b);
   }

   /** Multiplication of fractions.
    * @param m second factor
    * @return this*m
    */
   public Lfraction times (Lfraction m) {
      long a = numerator * m.numerator;
      long b = denominator * m.denominator;
      return new Lfraction(a, b);
   }

   /** Inverse of the fraction. n/d becomes d/n.
    * @return inverse of this fraction: 1/this
    */
   public Lfraction inverse() {
      if (numerator == 0) throw new RuntimeException("\nDenominator != 0");
      return new Lfraction(denominator, numerator);
   }

   /** Opposite of the fraction. n/d becomes -n/d.
    * @return opposite of this fraction: -this
    */
   public Lfraction opposite() {
      return new Lfraction(-numerator, denominator);
   }

   /** Difference of fractions.
    * @param m subtrahend
    * @return this-m
    */
   public Lfraction minus (Lfraction m) {
      return plus(m.opposite());
   }

   /** Quotient of fractions.
    * @param m divisor
    * @return this/m
    */
   public Lfraction divideBy (Lfraction m) {
      return times(m.inverse());
   }

   /** Comparision of fractions.
    * @param m second fraction
    * @return -1 if this < m; 0 if this==m; 1 if this > m
    */
   @Override
   public int compareTo (Lfraction m) {
      if (numerator * m.denominator < denominator * m.numerator) return -1;
      if (numerator * m.denominator > denominator * m.numerator) return 1;
      else return 0;
   }

   /** Clone of the fraction.
    * @return new fraction equal to this
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
      return new Lfraction(numerator, denominator);
   }

   /** Integer part of the (improper) fraction. 
    * @return integer part of this fraction
    */
   public long integerPart() {
      return numerator / denominator;
   }

   /** Extract fraction part of the (improper) fraction
    * (a proper fraction without the integer part).
    * @return fraction part of this fraction
    */
   public Lfraction fractionPart() {
      return new Lfraction(numerator % denominator, denominator);
   }

   /** Approximate value of the fraction.
    * @return numeric value of this fraction
    */
   public double toDouble() {
      return (double) numerator / denominator;
   }

   /** Double value f presented as a fraction with denominator d > 0.
    * @param f real number
    * @param d positive denominator for the result
    * @return f as an approximate fraction of form n/d
    */
   public static Lfraction toLfraction (double f, long d) {
      return new Lfraction(Math.round(f*d), d);
   }

   /** Conversion from string to the fraction. Accepts strings of form
    * that is defined by the toString method.
    * @param s string form (as produced by toString) of the fraction
    * @return fraction represented by s
    */
   public static Lfraction valueOf (String s) {
      if (s == null) throw new RuntimeException("\nInvalid input: " + s);

      String[] parts = s.split("\\s+");
      long resultNumerator;
      long resultDenominator;

      if (parts.length != 3) throw new RuntimeException("\nInvalid input: " + s);
      if(!parts[1].equals("/")) throw new RuntimeException("\nInvalid input: " + s);

      try {
         resultNumerator = Long.parseLong(parts[0]);
         resultDenominator = Long.parseLong(parts[2]);
      } catch (NumberFormatException | NullPointerException nfe) {
         throw new RuntimeException("\nInvalid input: " + s);
      }

      return new Lfraction(resultNumerator, resultDenominator);
   }

   public static long greatestCommonDivisor(long a, long b){
      return b == 0
              ? Math.abs(a)
              : greatestCommonDivisor(b, a%b);
   }
}
